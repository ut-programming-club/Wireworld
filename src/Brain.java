
public class Brain {
	private CellState[][][] grid;
	private int size;
	boolean currGrid;
	
	public Brain(int size){
		this.size = size;
		grid = new CellState[2][size][size];
		currGrid = true;

	}

	public void cycleCell(int x, int y){
		int i;
		if(currGrid)
			i = 0;
		else
			i = 1;
		
		switch (grid[i][x][y]) {
			case Empty:
				grid[i][x][y] = CellState.Conductor;
				break;
			case Conductor:
				grid[i][x][y] = CellState.Head;
				break;
			case Head:
				grid[i][x][y] = CellState.Tail;
				break;
			case Tail:
				grid[i][x][y] = CellState.Empty;
				break;
		}
	}
	
	public CellState[][] returnGrid(){
		int i;
		if(currGrid)
			i = 0;
		else
			i = 1;
		return grid[i];
	}
	
	public void step(){
		int k;
		if(currGrid) // k gives the opposite grid that currGrid represents
			k = 1;
		else 
			k = 0;
		
		for (int i = 0; i < size; i++){
			for( int j = 0; j < size; j++){
				grid[k][i][j] = fateOfCell(i , j);
			}
		}
		currGrid = !currGrid;
		
	}
	
	private CellState fateOfCell(int x, int y){ 
		int i;
		if(currGrid)
			i = 0;
		else
			i = 1;
		switch (grid[i][x][y]) {
			case Empty:
				return CellState.Empty;
			case Tail:
				return CellState.Conductor;
			case Head:
				return CellState.Tail;
			case Conductor:
				switch (numAdjacentHeads(x,y)) {
					case 1: case 2:
						return CellState.Head;
					default:
						return CellState.Conductor;	
				}
		}
		
		return null;
    }
	
	private int numAdjacentHeads(int x, int y){
		int i;
		if(currGrid)
			i = 0;
		else
			i = 1;
		int adjacent = 0;
		if (grid[i][((x-1)%size + size)%size][((y-1)%size + size)%size] == CellState.Head) {
		    adjacent++;
		}
		if (grid[i][((x-1)%size + size)%size][(y)%size] == CellState.Head) {
		    adjacent++;
		}
		if (grid[i][((x-1)%size + size)%size][(y+1)%size] == CellState.Head) {
		    adjacent++;
		}
		if (grid[i][(x)%size][((y-1)%size + size)%size] == CellState.Head) {
		    adjacent++;
		}
		if (grid[i][(x)%size][(y+1)%size] == CellState.Head) {
		    adjacent++;
		}
		if (grid[i][(x+1)%size][((y-1)%size + size)%size] == CellState.Head) {
		    adjacent++;
		}
		if (grid[i][(x+1)%size][(y)%size] == CellState.Head) {
		    adjacent++;
		}
		if (grid[i][(x+1)%size][(y+1)%size] == CellState.Head) {
		    adjacent++;
		}
		return adjacent;
	}
	
	
}
