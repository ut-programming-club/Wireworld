
import javafx.scene.paint.Color;

public enum CellState {
	Empty(Color.BLACK),
	Conductor(Color.GOLD),
	Head(Color.RED),
	Tail(Color.BLUE);
	
	Color color;
	
	CellState(Color c){
		color = c;
	}
	
}
