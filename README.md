This is Wireworld.
Wireworld is a cellular automaton which has simple rules, but is Turing-complete.
The cells in Wireworld can be in four different states:
Empty
Conductor
Electron Head
Electron Tail
The simple rules are:
Empty -> Empty
Electron Head -> Electron Tail
Electron Tail -> Conductor
Conductor -> Electron Head if exactly 1 or 2 neighbors
Else Conductor -> Conductor