package main;

import javafx.scene.paint.Color;

public class Brain {
	private CellState[][] grid;
	private int width;
	private int height;
	
	public Brain(long width, long height) {
		this.width = (int) width;
		this.height = (int) height;
		this.grid = new CellState[this.width][this.height];
	}
	
	public Brain(long gridSize) {
		this.width = (int) gridSize;
		this.height = (int) gridSize;
		this.grid = new CellState[this.width][this.height];
	}
	
	public CellState[][] getGrid() {
		return grid;
	}
	
	public int getWidth() {
		return width;
	}
	
	public int getHeight() {
		return height;
	}
	
	public void setCellToState(int x, int y, CellState s) {
		grid[x][y]=s;
	}
	
	public Color cycleCellState(int x, int y) {
		switch(grid[x][y]) {
			case EMPTY: 
				grid[x][y] = CellState.CONDUCTOR;
				break;
			case CONDUCTOR: 
				grid[x][y] = CellState.HEAD;
				break;
			case HEAD: 
				grid[x][y] = CellState.TAIL;
				break;
			case TAIL: 
				grid[x][y] = CellState.EMPTY;
				break;
			default:
				grid[x][y] = CellState.EMPTY;
		}
		return grid[x][y].c;
	}
	
	public void step() {
		CellState[][] temp = new CellState[width][height];
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				temp[i][j] = fateOfCell(i,j);
			}
		}
		grid = temp;
	}
	
	private CellState fateOfCell(int x, int y) {
		switch(grid[x][y]) {
			case EMPTY:
				return CellState.EMPTY;
			case HEAD:
				return CellState.TAIL;
			case TAIL:
				return CellState.CONDUCTOR;
			case CONDUCTOR:
				if(numHeadNeighbors(x,y)==1 | numHeadNeighbors(x,y)==2) {
					return CellState.HEAD;
				}
				else {
					return CellState.CONDUCTOR;
				}
			default:
				return CellState.EMPTY;
		}
	}
	
	private int numHeadNeighbors(int x, int y) {
		int n = 0;
		for(int dx = -1; dx <= 1; dx++) {
			for(int dy = -1; dy <= 1; dy++) {
				if(dx==0 & dy==0) continue;
				else {
					int nx = x + dx + width;
					int ny = y + dy + height;
					nx %= width;
					ny %= height;
					if(grid[nx][ny]==CellState.HEAD) n++;
				}
			}
		}
		return n;
	}
	
	public void clear() {
		for(int i = 0; i < width; i++) {
			for(int j = 0; j < height; j++) {
				grid[i][j] = CellState.EMPTY;
			}
		}
	}
}
