package main;

import java.util.Timer;
import java.util.TimerTask;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class View extends Application {
	private static final int GRIDSIZE = 75;
	
	private Brain game = new Brain(GRIDSIZE);
	private Rectangle[][] grid = new Rectangle[GRIDSIZE][GRIDSIZE];
	private int gen = 0;
	private Text gens = new Text("Generation 0");
	private Timer timer;
	private boolean isRunning = false;
	@Override
	public void start(Stage primaryStage) throws Exception {
		Alert startSim = new Alert(AlertType.INFORMATION);
		startSim.setTitle("Wireworld: A Turing-Complete Cellular Automaton");
		startSim.setHeaderText("Welcome to Wireworld!");
		startSim.setContentText("Welcome to Wireworld! Here are a few things you should know:\nWireworld's"
				+ " cells can be in four different states: empty, conductor, electron head, and electron tail."
				+ " The coloring is as follows:\nEmpty: Black\nConductor: Gold\nElectron Head: Blue\nElectron Tail: Red\n"
				+ "Click on an empty cell to change it to conductor. Clicking again will change it to an electron head,"
				+ " while clicking on the electron head will change it to an electron tail. Clicking on the cell again will"
				+ " change it back to an empty cell.\nHave fun!");
		startSim.showAndWait();
		Group cells = new Group();
		for(int i = 0; i < GRIDSIZE; i++) {
			for(int j = 0; j < GRIDSIZE; j++) {
				final int x = i;
				final int y = j;
				grid[i][j] = new Rectangle();
				grid[i][j].setX((900/GRIDSIZE)*i + 1);
				grid[i][j].setY((900/GRIDSIZE)*j + 1);
				grid[i][j].setWidth((1000/GRIDSIZE)-2);
				grid[i][j].setHeight((1000/GRIDSIZE)-2);
				grid[i][j].setStrokeWidth(0.5);
				grid[i][j].setFill(Color.BLACK);
				game.setCellToState(x, y, CellState.EMPTY);
				grid[i][j].setOnMouseClicked((MouseEvent me) -> {
					grid[x][y].setFill(game.cycleCellState(x, y));
				});
				cells.getChildren().add(grid[i][j]);
			}
		}
		Button step = new Button("Step");
		step.setOnAction((ActionEvent e) -> {
			step();
		});
		
		Button run = new Button("Run");
		run.setOnAction((ActionEvent e) -> {
			if(!isRunning) {
				isRunning = true;
				new Thread() {
					@Override
					public void run() {
						timer = new Timer();
						timer.scheduleAtFixedRate(new TimerTask() {
							@Override
							public void run() {
								Platform.runLater(() -> {
									step();
								});
							}
						}, 0, 100);
					}
				}.start();
			}
		});
		
		Button stop = new Button("Stop");
		stop.setOnAction((ActionEvent) -> {
			if(isRunning) timer.cancel();
			isRunning = false;
		});
		
		Button clear = new Button("Clear");
		clear.setOnAction((ActionEvent) -> {
			clear();
		});
		
		HBox top = new HBox();
		top.setPadding(new Insets(15,12,15,12));
		top.setSpacing(10);
		top.setStyle("-fx-background-color: Aqua");
		top.getChildren().addAll(step, run, stop, clear, gens);
		
		BorderPane root = new BorderPane();
		root.setTop(top);
		root.setCenter(cells);
		root.setStyle("-fx-background-color:MediumSeaGreen");
		Scene scene = new Scene(root, 1080, 1080);
		
		primaryStage.setTitle("Wireworld");
		primaryStage.setScene(scene);
		primaryStage.show();
	}
	
	private void step() {
		game.step();
		CellState[][] stepped = game.getGrid();
		for(int i = 0; i < GRIDSIZE; i++) {
			for(int j = 0; j < GRIDSIZE; j++) {
				grid[i][j].setFill(stepped[i][j].c);
			}
		}
		gen++;
		gens.setText("Generation "+gen);
	}
	
	private void clear() {
		game.clear();
		gen = 0;
		gens.setText("Generation 0");
		for(int i = 0; i < GRIDSIZE; i++) {
			for(int j = 0; j < GRIDSIZE; j++) {
				grid[i][j].setFill(Color.BLACK);
			}
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		launch(args);
	}

}
