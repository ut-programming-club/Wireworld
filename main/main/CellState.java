package main;

import javafx.scene.paint.Color;

public enum CellState {
	EMPTY(Color.BLACK),
	CONDUCTOR(Color.GOLD),
	HEAD(Color.BLUE),
	TAIL(Color.RED);
	final Color c;
	CellState(Color c) {
		this.c = c;
	}
}
